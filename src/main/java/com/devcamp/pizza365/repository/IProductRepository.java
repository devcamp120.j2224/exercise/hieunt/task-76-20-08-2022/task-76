package com.devcamp.pizza365.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.pizza365.entity.Product;

public interface IProductRepository extends JpaRepository <Product, Long> {
    @Query(value = "SELECT * FROM products WHERE product_name LIKE :productName%", nativeQuery = true)
	List<Product> findByProductNameLike(@Param("productName") String productName);

	@Query(value = "SELECT * FROM products WHERE product_description LIKE :productDescription%", nativeQuery = true)
	List<Product> findByProductDescriptionLike(@Param("productDescription") String productDescription);

	@Query(value = "SELECT * FROM products WHERE product_vendor LIKE %:productVendor%", nativeQuery = true)
	List<Product> findByProductVendorLike(@Param("productVendor") String productVendor, Pageable pageable);

	@Query(value = "SELECT * FROM products WHERE product_code LIKE %:productCode", nativeQuery = true)
	List<Product> findByProductCodeLike(@Param("productCode") String productCode, Pageable pageable);

	@Query(value = "SELECT * FROM products WHERE product_scale = :product_scale ORDER BY product_name ASC", nativeQuery = true)
	List<Product> findByProductScaleLike(@Param("product_scale") String product_scale, Pageable pageable);

	@Transactional
	@Modifying
	@Query(value = "UPDATE products SET product_scale = :product_scale  WHERE product_scale IS null", nativeQuery = true)
	int updateProductScale(@Param("product_scale") String product_scale);
}
