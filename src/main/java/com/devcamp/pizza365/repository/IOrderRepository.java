package com.devcamp.pizza365.repository;

import java.sql.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcamp.pizza365.entity.Order;


@Repository
public interface IOrderRepository extends JpaRepository<Order, Long> {
	Order findByOrderCode(String orderId);

	@Query(value = "SELECT * FROM orders WHERE order_date LIKE :orderDate%", nativeQuery = true)
	List<Order> findByOrderDateLike(@Param("orderDate") Date orderDate);

	@Query(value = "SELECT * FROM orders WHERE required_date LIKE :requiredDate%", nativeQuery = true)
	List<Order> findByRequireDateLike(@Param("requiredDate") Date requiredDate);

	@Query(value = "SELECT * FROM orders WHERE status LIKE :status%", nativeQuery = true)
	List<Order> findByStatusLike(@Param("status") String status, Pageable pageable);

	@Query(value = "SELECT * FROM orders WHERE comments = :comments ORDER BY status ASC", nativeQuery = true)
	List<Order> findByCommentLike(@Param("comments") String comments, Pageable pageable);

	@Transactional
	@Modifying
	@Query(value = "UPDATE orders SET comments = :comments  WHERE comments IS null", nativeQuery = true)
	int updateComment(@Param("comments") String comments);
}
