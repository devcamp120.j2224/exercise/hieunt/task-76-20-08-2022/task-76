package com.devcamp.pizza365.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.pizza365.entity.Office;

public interface IOfficeRepository extends JpaRepository <Office, Long> {
    @Query(value = "SELECT * FROM offices WHERE city LIKE :city%", nativeQuery = true)
	List<Office> findByCityLike(@Param("city") String city);

	@Query(value = "SELECT * FROM offices WHERE phone LIKE :phone%", nativeQuery = true)
	List<Office> findByPhoneLike(@Param("phone") String phone);

	@Query(value = "SELECT * FROM offices WHERE address_line LIKE %:addressLine%", nativeQuery = true)
	List<Office> findByAddressLineLike(@Param("addressLine") String addressLine, Pageable pageable);

	@Query(value = "SELECT * FROM offices WHERE state LIKE %:state", nativeQuery = true)
	List<Office> findByStateLike(@Param("state") String state, Pageable pageable);

	@Query(value = "SELECT * FROM offices WHERE country = :country ORDER BY city ASC", nativeQuery = true)
	List<Office> findByCountryOrderByCity(@Param("country") String country, Pageable pageable);

	@Transactional
	@Modifying
	@Query(value = "UPDATE offices SET country = :country  WHERE country IS null", nativeQuery = true)
	int updateCountry(@Param("country") String country);
}
