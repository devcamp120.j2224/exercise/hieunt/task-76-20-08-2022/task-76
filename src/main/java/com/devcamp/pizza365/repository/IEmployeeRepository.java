package com.devcamp.pizza365.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.pizza365.entity.Employee;

public interface IEmployeeRepository extends JpaRepository <Employee, Long> {
    @Query(value = "SELECT * FROM employees WHERE last_name LIKE :lastName%", nativeQuery = true)
	List<Employee> findByLastNameLike(@Param("lastName") String lastName);

	@Query(value = "SELECT * FROM employees WHERE first_name LIKE :firstName%", nativeQuery = true)
	List<Employee> findByFirstNameLike(@Param("firstName") String firstName);

	@Query(value = "SELECT * FROM employees WHERE email LIKE %:email%", nativeQuery = true)
	List<Employee> findByEmailLike(@Param("email") String email, Pageable pageable);

	@Query(value = "SELECT * FROM employees WHERE job_title LIKE %:jobTitle", nativeQuery = true)
	List<Employee> findByJobTitleLike(@Param("jobTitle") String jobTitle, Pageable pageable);

	@Query(value = "SELECT * FROM employees WHERE report_to = :report_to ORDER BY last_name ASC", nativeQuery = true)
	List<Employee> findByReportToOrderByLastName(@Param("report_to") int report_to, Pageable pageable);

	@Transactional
	@Modifying
	@Query(value = "UPDATE employees SET job_title = :job_title  WHERE job_title IS null", nativeQuery = true)
	int updateJobTitle(@Param("job_title") String job_title);

}
