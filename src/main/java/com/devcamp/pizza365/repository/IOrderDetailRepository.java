package com.devcamp.pizza365.repository;

import java.util.List;


import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.pizza365.entity.OrderDetail;

public interface IOrderDetailRepository extends JpaRepository <OrderDetail, Long> {
    @Query(value = "SELECT * FROM order_details WHERE order_id LIKE :orderId%", nativeQuery = true)
	List<OrderDetail> findByOrderIdLike(@Param("orderId") int orderId);

	@Query(value = "SELECT * FROM order_details WHERE product_id LIKE %:productId", nativeQuery = true)
	List<OrderDetail> findByProductIdLike(@Param("productId") int productId);

	@Query(value = "SELECT * FROM order_details WHERE quantity_order LIKE :quantityOrder%", nativeQuery = true)
	List<OrderDetail> findByQuantityOrderLike(@Param("quantityOrder") int quantityOrder, Pageable pageable);

	@Query(value = "SELECT * FROM order_details WHERE price_each = :price_each ORDER BY order_id ASC", nativeQuery = true)
	List<OrderDetail> findByPriceEachLike(@Param("price_each") int price_each, Pageable pageable);
}
