package com.devcamp.pizza365.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.pizza365.entity.ProductLine;

public interface IProductLineRepository extends JpaRepository <ProductLine,Long>  {
    @Query(value = "SELECT * FROM product_lines WHERE product_line LIKE :productLine%", nativeQuery = true)
	List<ProductLine> findByProductLineLike(@Param("productLine") String productLine);

	@Query(value = "SELECT * FROM product_lines WHERE description LIKE :description%", nativeQuery = true)
	List<ProductLine> findByDescriptionLike(@Param("description") String description);


	@Query(value = "SELECT * FROM product_lines WHERE description = :description ORDER BY product_line ASC", nativeQuery = true)
	List<ProductLine> findByDescriptionOrderByProductLine(@Param("description") String description, Pageable pageable);

	@Transactional
	@Modifying
	@Query(value = "UPDATE product_lines SET description = :description  WHERE description IS null", nativeQuery = true)
	int updateDescription(@Param("description") String description);
}
