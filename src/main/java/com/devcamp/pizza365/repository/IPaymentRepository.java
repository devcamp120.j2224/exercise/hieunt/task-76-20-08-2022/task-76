package com.devcamp.pizza365.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.pizza365.entity.Payment;

public interface IPaymentRepository extends JpaRepository <Payment, Long> {

    @Query(value = "SELECT * FROM payments WHERE check_number LIKE :cNumber%", nativeQuery = true)
	List<Payment> findByCheckNumberLike(@Param("number") String cNumber);

	@Query(value = "SELECT * FROM payments WHERE payment_date LIKE :paymentDate%", nativeQuery = true)
	List<Payment> findByPaymentDateLike(@Param("paymentDate") Date paymentDate);

	@Query(value = "SELECT * FROM payments WHERE customer_id = :customer_id ORDER BY check_number ASC", nativeQuery = true)
	List<Payment> findByCustomerIdLike(@Param("customer_id") Long customerId, Pageable pageable);

	@Transactional
	@Modifying
	@Query(value = "UPDATE payments SET payment_date = :paymentDate WHERE payment_date IS null", nativeQuery = true)
	int updateCountry(@Param("paymentDate") Date paymentDate);
    
}
