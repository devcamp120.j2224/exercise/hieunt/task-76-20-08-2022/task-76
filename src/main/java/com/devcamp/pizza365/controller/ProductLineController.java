package com.devcamp.pizza365.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.ProductLine;
import com.devcamp.pizza365.repository.IProductLineRepository;


@CrossOrigin
@RestController
@RequestMapping("/")
public class ProductLineController {
    @Autowired
    IProductLineRepository iProductLineRepository;

    @GetMapping("/productlines")
	public List<ProductLine> getAllProductLine() {
		return iProductLineRepository.findAll();
	}
	
	@GetMapping("/productline/details/{id}")
	public ResponseEntity<Object> getProductLineById(@PathVariable Long id) {
		Optional<ProductLine> productlineData = iProductLineRepository.findById(id);
		if (productlineData.isPresent())
			return new ResponseEntity<>(productlineData.get(), HttpStatus.OK);
		else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	 
	@PostMapping("/productline/create")
	public ResponseEntity<Object> createProductLine(@RequestBody ProductLine cProductLine) {
		try {
			ProductLine newProductLine = new ProductLine();
			newProductLine.setDescription(cProductLine.getDescription());
			newProductLine.setProductLine(cProductLine.getProductLine());
			newProductLine.setProducts(cProductLine.getProducts());
			ProductLine savedProductLine = iProductLineRepository.save(cProductLine);
			return new ResponseEntity<>(savedProductLine, HttpStatus.CREATED);
		} catch (Exception e) {
			return ResponseEntity.unprocessableEntity()
				.body("Failed to Create ProductLine: "+e.getCause().getCause().getMessage());
		}
	}
	
	@PutMapping("/productline/update/{id}")
	public ResponseEntity<Object> updateProductLine(@PathVariable("id") Long id, @RequestBody ProductLine cProductLine) {
		Optional<ProductLine> productlineData = iProductLineRepository.findById(id);
		if (productlineData.isPresent()) {
			try {
				ProductLine newProductLine = productlineData.get();
				newProductLine.setDescription(cProductLine.getDescription());
				newProductLine.setProductLine(cProductLine.getProductLine());
				newProductLine.setProducts(cProductLine.getProducts());
				ProductLine savedProductLine = iProductLineRepository.save(newProductLine);
				return new ResponseEntity<>(savedProductLine, HttpStatus.OK);
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity()
					.body("Failed to Update ProductLine: "+e.getCause().getCause().getMessage());
			}
		} else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);	
	}
	
	
	@DeleteMapping("/productline/delete/{id}")
	public ResponseEntity<Object> deleteProductLineById(@PathVariable Long id) {
		try {
			iProductLineRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println("========="+e.getCause().getCause().getMessage());
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
