package com.devcamp.pizza365.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.Employee;
import com.devcamp.pizza365.repository.IEmployeeRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class EmployeeController {
    @Autowired
    IEmployeeRepository iEmployeeRepository;

    @GetMapping("/employees")
	public List<Employee> getAllEmployee() {
		return iEmployeeRepository.findAll();
	}
	
	@GetMapping("/employee/details/{id}")
	public Object getEmployeeById(@PathVariable Long id) {
		if (iEmployeeRepository.findById(id).isPresent())
			return iEmployeeRepository.findById(id).get();
		else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
	@PostMapping("/employee/create")
	public ResponseEntity<Object> createEmployee(@RequestBody Employee cEmployee) {
		try {
			Employee newEmployee = new Employee();
			newEmployee.setFirstName(cEmployee.getFirstName());
			newEmployee.setLastName(cEmployee.getLastName());
			newEmployee.setExtension(cEmployee.getExtension());
			newEmployee.setEmail(cEmployee.getEmail());
			newEmployee.setReportTo(cEmployee.getReportTo());
			newEmployee.setOfficeCode(cEmployee.getOfficeCode());
			Employee savedEmployee = iEmployeeRepository.save(newEmployee);
			return new ResponseEntity<>(savedEmployee, HttpStatus.CREATED);
		} catch (Exception e) {
			return ResponseEntity.unprocessableEntity()
				.body("Failed to Create Employee: "+e.getCause().getCause().getMessage());
		}
	}
	
	@PutMapping("/employee/update/{id}")
	public ResponseEntity<Object> updateEmployee(@PathVariable("id") Long id, @RequestBody Employee cEmployee) {
		Optional<Employee> employeeData = iEmployeeRepository.findById(id);
		if (employeeData.isPresent()) {
			try {
				Employee newEmployee = employeeData.get();			
				newEmployee.setFirstName(cEmployee.getFirstName());
				newEmployee.setLastName(cEmployee.getLastName());
				newEmployee.setExtension(cEmployee.getExtension());
				newEmployee.setEmail(cEmployee.getEmail());
				newEmployee.setReportTo(cEmployee.getReportTo());
				newEmployee.setOfficeCode(cEmployee.getOfficeCode());
				newEmployee.setJobTitle(cEmployee.getJobTitle());			
				Employee savedEmployee = iEmployeeRepository.save(newEmployee);
				return new ResponseEntity<>(savedEmployee, HttpStatus.OK);
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity()
					.body("Failed to Update Employee: "+e.getCause().getCause().getMessage());
			} 
		} else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
	
	@DeleteMapping("/employee/delete/{id}")
	public ResponseEntity<Object> deleteEmployeeById(@PathVariable Long id) {
		try {
			iEmployeeRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println("===="+e.getCause().getCause().getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
