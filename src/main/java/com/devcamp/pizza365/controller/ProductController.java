package com.devcamp.pizza365.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.Product;
import com.devcamp.pizza365.entity.ProductLine;
import com.devcamp.pizza365.repository.IProductLineRepository;
import com.devcamp.pizza365.repository.IProductRepository;

@RestController
@CrossOrigin
@RequestMapping
public class ProductController {
    @Autowired
    IProductLineRepository iProductLineRepository;

    @Autowired
    IProductRepository iProductRepository;

    @GetMapping("/products")
	public List<Product> getAllProducts() {
		return iProductRepository.findAll();
	}

	@GetMapping("/product/details/{id}")
	public ResponseEntity<Object> getProductById(@PathVariable Long id) {
		Optional<Product> productData = iProductRepository.findById(id);
		if (productData.isPresent())
			return new ResponseEntity<>(productData.get(), HttpStatus.OK);
		else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@DeleteMapping("/product/delete/{id}")
	public ResponseEntity<Object> deleteProductById(@PathVariable Long id) {
		try {
			iProductRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/product/update/{id}")
	public ResponseEntity<Object> updateProduct(@PathVariable("id") Long id, @RequestBody Product cProduct) {
		Optional<Product> productData = iProductRepository.findById(id);
		if (productData.isPresent()) {
			Product newProduct = productData.get();
			newProduct.setProductCode(cProduct.getProductCode());
			newProduct.setProductName(cProduct.getProductName());
			newProduct.setProductDescription(cProduct.getProductDescription());
			newProduct.setProductScale(cProduct.getProductScale());
			newProduct.setProductVendor(cProduct.getProductVendor());
			newProduct.setQuantityInStock(cProduct.getQuantityInStock());
			newProduct.setBuyPrice(cProduct.getBuyPrice());
			newProduct.setOrderDetails(cProduct.getOrderDetails());
			return new ResponseEntity<>(iProductRepository.save(newProduct), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/product/byproductlineid/{id}")
	public List<Product> getProductByProductLineId(@PathVariable("id") Long id) {
		Optional<ProductLine> productLineData = iProductLineRepository.findById(id);
		if (productLineData.isPresent()) {
			return productLineData.get().getProducts();
		} else {
			return null;
		}
	}

	@PostMapping("/product/create/{id}")
	public ResponseEntity<Object> createProduct(@PathVariable("id") Long id, @RequestBody Product cProduct) {
		Optional<ProductLine> productLineData = iProductLineRepository.findById(id);
		if (productLineData.isPresent()) {
			Product newProduct = new Product();
			newProduct.setProductCode(cProduct.getProductCode());
			newProduct.setProductName(cProduct.getProductName());
			newProduct.setProductDescription(cProduct.getProductDescription());
			newProduct.setProductScale(cProduct.getProductScale());
			newProduct.setProductVendor(cProduct.getProductVendor());
			newProduct.setQuantityInStock(cProduct.getQuantityInStock());
			newProduct.setBuyPrice(cProduct.getBuyPrice());
			newProduct.setOrderDetails(cProduct.getOrderDetails());
			ProductLine _productLine = productLineData.get();
            newProduct.setProductLine(_productLine);
			return new ResponseEntity<>(iProductRepository.save(newProduct), HttpStatus.CREATED);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
