package com.devcamp.pizza365.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.Office;
import com.devcamp.pizza365.repository.IOfficeRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class OfficeController {
    @Autowired
    IOfficeRepository iOfficeRepository;

    @GetMapping("/offices")
	public List<Office> getAllOffice() {
		return iOfficeRepository.findAll();
	}
	
	@GetMapping("/office/details/{id}")
	public Object getOfficeById(@PathVariable Long id) {
		if (iOfficeRepository.findById(id).isPresent())
			return iOfficeRepository.findById(id).get();
		else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	 
	@PostMapping("/office/create")
	public ResponseEntity<Object> createOffice(@RequestBody Office cOffice) {
		try {
			Office newOffice = new Office();
			newOffice.setAddressLine(cOffice.getAddressLine());
			newOffice.setCity(cOffice.getCity());
			newOffice.setCountry(cOffice.getCountry());
			newOffice.setPhone(cOffice.getPhone());
			newOffice.setState(cOffice.getState());
			newOffice.setTerritory(cOffice.getTerritory());
			Office savedOffice = iOfficeRepository.save(cOffice);
			return new ResponseEntity<>(savedOffice, HttpStatus.CREATED);
		} catch (Exception e) {
			return ResponseEntity.unprocessableEntity()
			.body("Failed to Create Office: "+e.getCause().getCause().getMessage());
		}
	}
	 
	@PutMapping("/office/update/{id}")
	public ResponseEntity<Object> updateOffice(@PathVariable("id") Long id, @RequestBody Office cOffice) {
		Optional<Office> officeData = iOfficeRepository.findById(id);
		if (officeData.isPresent()) {
			try {
				Office newOffice = officeData.get(); 
				newOffice.setAddressLine(cOffice.getAddressLine());
				newOffice.setCity(cOffice.getCity());
				newOffice.setCountry(cOffice.getCountry());
				newOffice.setPhone(cOffice.getPhone());
				newOffice.setState(cOffice.getState());
				newOffice.setTerritory(cOffice.getTerritory()); 
				Office savedOffice = iOfficeRepository.save(newOffice);
				return new ResponseEntity<>(savedOffice, HttpStatus.OK);
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity()
				.body("Failed to Update Office: "+e.getCause().getCause().getMessage());
			}
		}  else 
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
	
	@DeleteMapping("/office/delete/{id}")
	public ResponseEntity<Object> deleteOfficeById(@PathVariable Long id) {
		try {
			iOfficeRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println("===="+e.getCause().getCause().getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
